<?php

$form['skin'] = [
  '#type' => 'details',
  '#title' => t('Skin'),
  '#group' => 'configuration',
  '#tree' => TRUE,
];

$form['skin']['main'] = [
  '#type' => 'select',
  '#title' => t('Main skin'),
  '#options' => [
    '' => t('No skin'),
    'light-blue' => t('Light Blue'),
  ],
  '#default_value' => theme_get_setting('skin.main', $theme),
  '#group' => 'container',
];
