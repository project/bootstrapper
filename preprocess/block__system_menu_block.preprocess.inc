<?php

/**
 * Implements hook_preprocess_HOOK().
 *
 * @param $variables
 */
function bootstrapper_preprocess_block__system_menu_block(&$variables) {
  // Add 'id' attribute to system blocks.
  if (!isset($variables['attributes']['id'])) {
    if (isset($variables['plugin_id'])) {
      $variables['attributes']['id'] = str_replace('-', '_', str_replace(':', '__', $variables['plugin_id']));
    }
  }
}
