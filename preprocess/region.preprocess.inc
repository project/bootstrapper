<?php

use Drupal\Core\Template\Attribute;

/**
 * Implements hook_preprocess_HOOK().
 *
 * @param $variables
 */
function bootstrapper_preprocess_region(&$variables) {
  if (!isset($variables['attributes_wrapper']) || !$variables['attributes_wrapper'] instanceof Attribute) {
    $variables['attributes_wrapper'] = new Attribute();
  }
  if (!isset($variables['attributes_body']) || !$variables['attributes_body'] instanceof Attribute) {
    $variables['attributes_body'] = new Attribute();
  }
}
