<?php

/**
 * Implements hook_preprocess_HOOK().
 *
 * @param $variables
 */
function bootstrapper_preprocess_page(&$variables) {
  // Remove messages from page render array.
  if (isset($variables['page']['content']['messages'])) {
    unset($variables['page']['content']['messages']);
  }
}
