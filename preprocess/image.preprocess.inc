<?php

/**
 * Implements hook_preprocess_HOOK().
 *
 * @param $variables
 */
function bootstrapper_preprocess_image(&$variables) {
  if (isset($variables['theme']['settings']['images']['responsive']) && $variables['theme']['settings']['images']['responsive']) {
    if (isset($variables['attributes']['class']) && is_array($variables['attributes']['class'])) {
      $variables['attributes']['class'][] = 'img-fluid';
    }
    elseif (!isset($variables['attributes']['class'])) {
      $variables['attributes']['class'][] = 'img-fluid';
    }
  }

  // Process SVG images. Add a special class.
  if (strtolower(pathinfo($variables['uri'], PATHINFO_EXTENSION)) == 'svg') {
    if (is_array($variables['attributes']['class'])) {
      $variables['attributes']['class'][] = 'img-svg';
    }
    else {
      $variables['attributes']['class'] .= ' img-svg';
    }
  }

}
