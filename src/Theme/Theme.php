<?php

namespace Drupal\bootstrapper\Theme;

use Drupal\Core\Extension\ThemeHandler;

/**
 * Utility class for managing the theme.
 */
class Theme {

  /**
   * Store ThemeHandler for quick access.
   *
   * @var ThemeHandler
   */
  protected $themeHandler;

  /**
   * The machine name of the active theme.
   *
   * @var string
   */
  protected $themeId;

  /**
   * Constructs a Theme object.
   *
   * @param string $theme_id
   *   The machine name of the active theme.
   */
  public function __construct($theme_id) {
    $this->themeId = $theme_id;
    $this->themeHandler = \Drupal::service('theme_handler');
  }

  /**
   * Returns the label of the current theme.
   *
   * @return string
   */
  protected function getThemeLabel() {
    return $this->themeHandler->getName($this->themeId);
  }

  /**
   * Builds the full theme trail (deepest base theme first) for a theme.
   *
   * @return array
   *   An array of all themes in the trail, keyed by theme key.
   */
  public function getThemeTrail() {
    if (($cache = &drupal_static(__FUNCTION__)) && isset($cache[$this->themeId])) {
      return $cache[$this->themeId];
    }

    $cache[$this->themeId] = array();

    $base_themes = $this->getBaseThemes();
    if (!empty($base_themes)) {
      $cache[$this->themeId] = $base_themes;
    }

    // Add our current subtheme ($key) to that array.
    $cache[$this->themeId][$this->themeId] = $this->getThemeLabel();

    return $cache[$this->themeId];
  }

  /**
   * Returns list of base themes for the current theme.
   *
   * @return array
   */
  protected function getBaseThemes() {
    return $this->themeHandler->getBaseThemes($this->themeHandler->listInfo(), $this->themeId);
  }

  /**
   * Includes preprocess hooks on behalf of a given theme.
   *
   * @param string $theme
   *   The name of the theme for which to register (pre-)process hooks.
   */
  public function includeFiles($theme) {
    foreach (['preprocess'] as $type) {
      // Iterate over all files in the current theme.
      foreach ($this->discoverFiles($theme, $type) as $item) {
        require_once($item->uri);
      }
    }
  }

  /**
   * Scans for files of a certain type in the given theme's path.
   *
   * @param string $theme
   *   The name of the theme scan.
   *
   * @param string $type
   *   The file type (e.g. 'preprocess') to scan for.
   *
   * @return array
   *   An array of file objects that matched the given type.
   */
  protected function discoverFiles($theme, $type) {
    $length = -(strlen($type) + 1);

    $path = \Drupal::service('extension.path.resolver')->getPath('theme', $theme);

    // Only look for files that match the 'something.preprocess.inc' pattern.
    $mask = '/.' . $type . '.inc$/';

    // Recursively scan the folder for the current step for (pre-)process
    // files and write them to the registry.
    $dir = $path . '/' . $type;
    $files = [];
    if (is_dir($dir)) {
      $files = \Drupal::service('file_system')->scanDirectory($dir, $mask);
    }
    foreach ($files as &$file) {
      $file->hook = strtr(substr($file->name, 0, $length), '-', '_');
    }

    return $files;
  }

}
