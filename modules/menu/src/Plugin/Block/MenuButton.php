<?php

namespace Drupal\bootstrapper_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * @Block(
 *   id = "bootstrapper_menu_button",
 *   admin_label = @Translation("Bootstrapper Menu Button"),
 *   category = @Translation("Bootstrapper")
 * )
 */
class MenuButton extends BlockBase {

/**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = array();

    $build['menu_button'] = array(
      '#theme' => 'bootstrapper_menu_button',
    );

    return $build;
  }

}
