(function () {
  'use strict';
  let htmlClasses = document.documentElement.classList;
  htmlClasses.remove('no-js');
  htmlClasses.add('js');
}());
